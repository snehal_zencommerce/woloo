-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 19, 2021 at 01:13 PM
-- Server version: 5.6.43-84.3
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isntml9v_woloo_shopping`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `date_time`, `status`) VALUES
(0, 'admin', 'admin', '2017-10-04 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(9) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `image` varchar(300) DEFAULT NULL,
  `banner1_image` varchar(300) DEFAULT NULL,
  `banner2_image` varchar(300) DEFAULT NULL,
  `banner3_image` varchar(300) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `image`, `banner1_image`, `banner2_image`, `banner3_image`, `date_time`, `status`) VALUES
(1, 'Hygiene', '161114046087441293-illustration-of-cleaning-symbol-on-hygiene-icon-.jpg', '1612173067WhatsApp_Image_2021-01-30_at_11.56.25_AM.jpeg', '1612173067WhatsApp_Image_2021-01-30_at_11.56.26_AM_(6).jpeg', '1612173067banner5.jpeg', '2021-02-01 09:51:07', 'Activated'),
(2, 'Health & Walleness', '1611990959105-1054131_student-health-center-white-x-red-background-clipart.png', '1612173106WhatsApp_Image_2021-01-30_at_11.56.26_AM_(1).jpeg', '1612173106WhatsApp_Image_2021-01-30_at_11.56.26_AM_(5).jpeg', '1612173106banner3.jpeg', '2021-02-01 09:51:46', 'Activated'),
(3, 'Fitness', '1611991038download.png', '1612173130WhatsApp_Image_2021-01-30_at_11.56.26_AM_(3).jpeg', '1612173130WhatsApp_Image_2021-01-30_at_11.56.26_AM_(4).jpeg', '1612173130banner2.jpeg', '2021-02-01 09:52:10', 'Activated'),
(4, 'Personal Care', '1612172201download_(1).png', '1612173156WhatsApp_Image_2021-01-30_at_11.56.26_AM_(5).jpeg', '1612173156WhatsApp_Image_2021-01-30_at_11.56.26_AM_(6).jpeg', '1612173156banner1.jpeg', '2021-02-01 09:52:36', 'Activated'),
(5, 'Fareness', '1612173018download_(1).png', '1612173018WhatsApp_Image_2021-01-30_at_11.56.25_AM.jpeg', '1612173018WhatsApp_Image_2021-01-30_at_11.56.26_AM_(1).jpeg', '1612173018banner4.jpeg', '2021-02-01 09:50:18', 'Activated'),
(6, 'Bagforever', '1613049691', '1613049691', '1613049691', '1613049691', '2021-02-11 13:21:31', 'Deactivated'),
(7, 'Test Category', '1615381459', '1615381459', '1615381459', '1615381459', '2021-03-10 13:04:19', NULL),
(8, 'Clothing', '1615534218ZC_150_x_150.png', '1615534321images_(1).jpeg', '1615534321images.jpeg', '1615534321download_(2).jpeg', '2021-03-12 07:32:01', 'Activated');

-- --------------------------------------------------------

--
-- Table structure for table `customer_address`
--

CREATE TABLE `customer_address` (
  `id` int(9) NOT NULL,
  `user_id` int(9) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flat_building` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `landmark` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_address`
--

INSERT INTO `customer_address` (`id`, `user_id`, `name`, `phone`, `pincode`, `city`, `state`, `area`, `flat_building`, `landmark`, `date_time`, `status`) VALUES
(1, 1, 'Arbaz Shaikh', '+91999999999', '401107', 'Mumbai', 'Maharashtra', 'Andheri (E)', '401 - Global City', 'Near Yazoo Park', NULL, NULL),
(2, 1, 'Arbaz Shaikh', '+91999999999', '401107', 'Mumbai', 'Maharashtra', 'Mira road(E)', '508 - Dhram Aprtment', 'Shanti marg', NULL, NULL),
(15, 199, 'Snehal S.', '', '400076', 'Mumbai', 'maharashtra', 'andheri', 'c9', 'test', '2021-02-25 12:06:32', NULL),
(17, 159, 'Snehal\n', '9320369229', '400076', 'Powai', 'Maharashtra', 'shopping complex Nerul', 'c9 neighborhood ', 'near SBI bank', '2021-03-11 10:36:19', NULL),
(18, 159, 'Snehal\n', '9320369229', '400076', 'Powai', 'Maharashtra', 'Powai iit', '56 Road', 'near dmart', '2021-03-11 10:36:49', NULL),
(19, 154, 'woloo test', '', '401108', 'mumbai', 'Maharashtra', 'Mira road', '406', 'abc', '2021-03-15 13:05:17', NULL),
(20, 154, 'null', 'null', '60001', 'mumbai', 'Maharashtra', 'Virar', '502, HK building', 'Virat Nagar', '2021-05-02 19:02:42', NULL),
(21, 44, 'Amit Arondekar', '9820800826', '400057', 'Mumbai', 'Maharashtra', 'Vile Parle East', '303, Datta Niwas, Ajmal Road', 'Rugvedi Bldg', '2021-05-03 19:01:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `excel_import_logs`
--

CREATE TABLE `excel_import_logs` (
  `id` int(9) NOT NULL,
  `user` varchar(30) DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `description` text,
  `type` varchar(40) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `excel_import_logs`
--

INSERT INTO `excel_import_logs` (`id`, `user`, `file_name`, `batch_id`, `description`, `type`, `date_time`, `status`) VALUES
(2, 'Admin', 'woloo_final_format.xlsx', NULL, 'Total uploaded records: 1 , Error In Rows:', NULL, '2021-05-11 11:58:39', NULL),
(3, 'Admin', 'woloo_final_format.xlsx', NULL, 'Total uploaded records: 1 , Error In Rows:', NULL, '2021-05-11 12:26:44', NULL),
(4, 'Admin', 'woloo_final_format.xlsx', NULL, 'Total uploaded records: 1 , Error In Rows:', '', '2021-05-11 12:30:58', NULL),
(5, 'Admin', 'woloo_final_format.xlsx', NULL, 'Total uploaded records: 0 , Error In Rows:1 Row 1 : Duplicate SKU', '', '2021-05-13 12:00:38', NULL),
(6, 'Admin', 'woloo_final_format.xlsx', NULL, 'Total uploaded records: 1 , Error In Rows:', '', '2021-05-13 12:03:44', NULL),
(7, 'Admin', 'woloo_final_format.xlsx', NULL, 'Total uploaded records: 0 , Error In Rows:1 Row 1 : Category name not valid', '', '2021-05-13 12:05:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `franchisee`
--

CREATE TABLE `franchisee` (
  `id` int(9) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_userid` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_pincodes` text COLLATE utf8_unicode_ci,
  `area` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `woloo_margin_per` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_margin_per` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `franchisee`
--

INSERT INTO `franchisee` (`id`, `name`, `image`, `app_userid`, `delivery_pincodes`, `area`, `woloo_margin_per`, `customer_margin_per`, `username`, `password`, `date_time`, `status`) VALUES
(1, 'Abc', '1619930635', '10', '401108,401107,600001', '', '10', '25', 'abc', '123', '2021-05-02 13:04:26', 'Activated');

-- --------------------------------------------------------

--
-- Table structure for table `home_banner`
--

CREATE TABLE `home_banner` (
  `id` int(9) NOT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_banner`
--

INSERT INTO `home_banner` (`id`, `image`, `date_time`, `status`) VALUES
(1, '1611988237banner1.jpeg', '2021-01-30 06:30:37', 'Deactivated'),
(2, '1611988249banner2.jpeg', '2021-01-30 06:30:49', 'Deactivated'),
(3, '1611988266banner3.jpeg', '2021-01-30 06:31:06', 'Deactivated'),
(4, '1611988299banner4.jpeg', '2021-01-30 06:31:39', 'Activated'),
(5, '1613051268banner_2.jpg', '2021-02-11 13:47:48', 'Activated');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(9) NOT NULL,
  `user_id` int(9) DEFAULT NULL,
  `user_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'customer',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_charges` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_point_used` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_data` longtext COLLATE utf8_unicode_ci,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `user_type`, `name`, `mobile`, `total_amount`, `shipping_charges`, `total_point_used`, `address`, `order_data`, `date_time`, `status`) VALUES
(3, 1, 'customer', 'Arbaz Shaikh', '9999999999', '270', NULL, NULL, '401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra', '{\"order\":[{\"user_id\":\"1\",\"name\":\"Arbaz Shaikh\",\"mobile\":\"9999999999\",\"address\":\"401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra\",\"total_amount\":\"270\"}],\"order_product\":[{\"pro_id\":\"4\",\"pro_name\":\"Product 4\",\"qty\":\"3\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"30\",\"270\":\"270\"}]}', '2021-02-11 11:35:32', NULL),
(4, 1, 'customer', 'Arbaz Shaikh', '9999999999', '180', NULL, NULL, '401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra', '{\"order\":[{\"user_id\":\"1\",\"name\":\"Arbaz Shaikh\",\"mobile\":\"9999999999\",\"address\":\"401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra\",\"total_amount\":\"180\"}],\"order_product\":[{\"pro_id\":\"16\",\"pro_name\":\"Product 4\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"180\":\"90\"},{\"pro_id\":\"3\",\"pro_name\":\"Product 3\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"180\":\"90\"}]}', '2021-02-11 15:45:37', NULL),
(5, 1, 'customer', 'Arbaz Shaikh', '9999999999', '450', NULL, NULL, 'powai , na , Powai , powai-400076 maharashtra', '{\"order\":[{\"user_id\":\"1\",\"name\":\"Arbaz Shaikh\",\"mobile\":\"9999999999\",\"address\":\"powai , na , Powai , powai-400076 maharashtra\",\"total_amount\":\"450\"}],\"order_product\":[{\"pro_id\":\"4\",\"pro_name\":\"Product 4\",\"qty\":\"2\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"20\",\"450\":\"180\"},{\"pro_id\":\"4\",\"pro_name\":\"Product 4\",\"qty\":\"3\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"30\",\"450\":\"270\"}]}', '2021-02-11 17:24:03', NULL),
(6, 1, 'customer', 'Arbaz Shaikh', '9999999999', '90', NULL, NULL, '401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra', '{\"order\":[{\"user_id\":\"1\",\"name\":\"Arbaz Shaikh\",\"mobile\":\"9999999999\",\"address\":\"401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra\",\"total_amount\":\"90\"}],\"order_product\":[{\"pro_id\":\"4\",\"pro_name\":\"Product 4\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"90\":\"90\"}]}', '2021-02-11 17:33:20', NULL),
(7, 1, 'customer', 'Arbaz Shaikh', '9999999999', '90', NULL, NULL, '401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra', '{\"order\":[{\"user_id\":\"1\",\"name\":\"Arbaz Shaikh\",\"mobile\":\"9999999999\",\"address\":\"401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra\",\"total_amount\":\"90\"}],\"order_product\":[]}', '2021-02-11 17:33:28', NULL),
(8, 1, 'customer', 'Arbaz Shaikh', '9999999999', '90', NULL, NULL, '401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra', '{\"order\":[{\"user_id\":\"1\",\"name\":\"Arbaz Shaikh\",\"mobile\":\"9999999999\",\"address\":\"401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra\",\"total_amount\":\"90\"}],\"order_product\":[{\"pro_id\":\"2\",\"pro_name\":\"Product 2\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"90\":\"90\"}]}', '2021-02-11 17:47:58', NULL),
(9, 1, 'customer', 'Arbaz Shaikh', '9999999999', '180', NULL, NULL, '401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra', '{\"order\":[{\"user_id\":\"1\",\"name\":\"Arbaz Shaikh\",\"mobile\":\"9999999999\",\"address\":\"401 - Global City , Near Yazoo Park , Andheri (E) , Mumbai-401107 Maharashtra\",\"total_amount\":\"180\"}],\"order_product\":[{\"pro_id\":\"10\",\"pro_name\":\"Product 2\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"180\":\"90\"},{\"pro_id\":\"10\",\"pro_name\":\"Product 2\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"180\":\"90\"}]}', '2021-02-11 17:55:52', NULL),
(10, 199, 'customer', 'Snehal S.', 'Powai', '', NULL, NULL, '90', '{\"order\":[{\"user_id\":\"199\",\"name\":\"Snehal S.\",\"address\":\"Powai\",\"total_amount\":\"90\"}],\"order_product\":[{\"pro_id\":\"8\",\"pro_name\":\"Product 4\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"90\":\"90\"}]}', '2021-02-22 16:18:09', NULL),
(11, 199, 'customer', 'Snehal S.', 'Powai', '', NULL, NULL, '190', '{\"order\":[{\"user_id\":\"199\",\"name\":\"Snehal S.\",\"address\":\"Powai\",\"total_amount\":\"190\"}],\"order_product\":[{\"pro_id\":\"9\",\"pro_name\":\"Product 1\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"0\",\"190\":\"100\"},{\"pro_id\":\"9\",\"pro_name\":\"Product 1\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"190\":\"90\"}]}', '2021-02-24 10:44:48', NULL),
(12, 199, 'customer', 'Snehal S.', 'Hiranandani Powai\n', '', NULL, NULL, '180', '{\"order\":[{\"user_id\":\"199\",\"name\":\"Snehal S.\",\"address\":\"Hiranandani Powai\n\",\"total_amount\":\"180\"}],\"order_product\":[{\"pro_id\":\"8\",\"pro_name\":\"Product 4\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"180\":\"90\"},{\"pro_id\":\"7\",\"pro_name\":\"Product 3\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"180\":\"90\"}]}', '2021-02-24 16:18:14', NULL),
(13, 199, 'customer', 'Snehal S.', 'Hiranandani Powai\n', '', NULL, NULL, '180', '{\"order\":[{\"user_id\":\"199\",\"name\":\"Snehal S.\",\"address\":\"Hiranandani Powai\n\",\"total_amount\":\"180\"}],\"order_product\":[]}', '2021-02-24 16:18:47', NULL),
(14, 199, 'customer', 'Snehal S.', 'Hiranandani Powai\n', '', NULL, NULL, '180', '{\"order\":[{\"user_id\":\"199\",\"name\":\"Snehal S.\",\"address\":\"Hiranandani Powai\n\",\"total_amount\":\"180\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"180\":\"489\"}]}', '2021-02-24 16:19:11', NULL),
(15, 199, 'customer', 'Snehal S.', 'Hiranandani Powai\n', '', NULL, NULL, '90', '{\"order\":[{\"user_id\":\"199\",\"name\":\"Snehal S.\",\"address\":\"Hiranandani Powai\n\",\"total_amount\":\"90\"}],\"order_product\":[{\"pro_id\":\"10\",\"pro_name\":\"Product 2\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"90\":\"90\"}]}', '2021-02-24 16:19:49', NULL),
(16, 154, 'customer', '978', 'null', '401 , Poonam complex , Mira road , Mumbai-401107 Maharashtra', NULL, NULL, 'support@arkloop.com', '{\"order\":[{\"user_id\":\"154\",\"total_amount\":\"978\",\"address\":\"401 , Poonam complex , Mira road , Mumbai-401107 Maharashtra\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"2\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"20\",\"978\":\"978\"}]}', '2021-02-24 17:19:00', NULL),
(17, 154, 'customer', 'null', 'null', '489', NULL, NULL, '401 , Poonam complex , Mira road , Mumbai-401107 Maharashtra', '{\"order\":[{\"user_id\":\"154\",\"name\":\"null\",\"total_amount\":\"489\",\"address\":\"401 , Poonam complex , Mira road , Mumbai-401107 Maharashtra\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 17:33:47', NULL),
(18, 154, 'customer', 'woloo test', 'null', '489', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"489\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 17:37:16', NULL),
(19, 154, 'customer', 'woloo test', 'support@arkloop.com', '90', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"90\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"4\",\"pro_name\":\"Product 4\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"90\":\"90\"}]}', '2021-02-24 17:43:14', NULL),
(20, 154, 'customer', 'woloo test', 'support@arkloop.com', '489', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"489\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 17:52:15', NULL),
(21, 154, 'customer', 'woloo test', 'support@arkloop.com', '489', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"489\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 17:55:11', NULL),
(22, 154, 'customer', 'woloo test', 'support@arkloop.com', '489', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"489\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 18:05:50', NULL),
(23, 154, 'customer', 'woloo test', 'support@arkloop.com', '489', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"489\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 18:07:34', NULL),
(24, 154, 'customer', 'woloo test', 'support@arkloop.com', '90', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"90\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"8\",\"pro_name\":\"Product 4\",\"qty\":\"1\",\"price\":\"100\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"90\":\"90\"}]}', '2021-02-24 18:08:34', NULL),
(25, 154, 'customer', 'woloo test', 'support@arkloop.com', '489', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"489\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 18:10:49', NULL),
(26, 154, 'customer', 'woloo test', 'support@arkloop.com', '489', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"489\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 18:19:43', NULL),
(27, 154, 'customer', 'woloo test', 'support@arkloop.com', '489', NULL, NULL, 'Mira road', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"489\",\"address\":\"Mira road\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"489\":\"489\"}]}', '2021-02-24 18:21:36', NULL),
(28, 154, 'customer', 'woloo test', 'support@arkloop.com', '589', '100', '10', '403 , h5 JK residency , naragani road , virar , mumbai-401108 Maharashtra', '{\"order\":[{\"user_id\":\"154\",\"name\":\"woloo test\",\"total_amount\":\"589\",\"address\":\"403 , h5 JK residency , naragani road , virar , mumbai-401108 Maharashtra\",\"email\":\"support@arkloop.com\",\"mobile\":\"null\",\"shipping_charges\":\"100\"}],\"order_product\":[{\"pro_id\":\"18\",\"pro_name\":\"Pee Safe - Toilet Seat Sanitizer Spray\",\"qty\":\"1\",\"price\":\"499\",\"customer_margin_per\":\"10\",\"point_used\":\"10\",\"589\":\"489\"}]}', '2021-03-04 18:00:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(9) NOT NULL,
  `order_id` int(9) DEFAULT NULL,
  `product_id` int(9) DEFAULT NULL,
  `pro_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `point_used` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `pro_price`, `qty`, `amount`, `point_used`, `date_time`, `status`) VALUES
(1, 3, 4, '100', '3', '270', '30', '2021-02-11 06:05:32', NULL),
(2, 4, 16, '100', '1', '90', '10', '2021-02-11 10:15:37', NULL),
(3, 4, 3, '100', '1', '90', '10', '2021-02-11 10:15:37', NULL),
(4, 5, 4, '100', '2', '180', '20', '2021-02-11 11:54:03', NULL),
(5, 5, 4, '100', '3', '270', '30', '2021-02-11 11:54:03', NULL),
(6, 6, 4, '100', '1', '90', '10', '2021-02-11 12:03:20', NULL),
(7, 8, 2, '100', '1', '90', '10', '2021-02-11 12:17:58', NULL),
(8, 9, 10, '100', '1', '90', '10', '2021-02-11 12:25:52', NULL),
(9, 9, 10, '100', '1', '90', '10', '2021-02-11 12:25:52', NULL),
(10, 10, 8, '100', '1', '90', '10', '2021-02-22 10:48:09', NULL),
(11, 11, 9, '100', '1', '100', '0', '2021-02-24 05:14:48', NULL),
(12, 11, 9, '100', '1', '90', '10', '2021-02-24 05:14:48', NULL),
(13, 12, 8, '100', '1', '90', '10', '2021-02-24 10:48:14', NULL),
(14, 12, 7, '100', '1', '90', '10', '2021-02-24 10:48:14', NULL),
(15, 14, 18, '499', '1', '489', '10', '2021-02-24 10:49:11', NULL),
(16, 15, 10, '100', '1', '90', '10', '2021-02-24 10:49:49', NULL),
(17, 16, 18, '499', '2', '978', '20', '2021-02-24 11:49:00', NULL),
(18, 17, 18, '499', '1', '489', '10', '2021-02-24 12:03:47', NULL),
(19, 18, 18, '499', '1', '489', '10', '2021-02-24 12:07:16', NULL),
(20, 19, 4, '100', '1', '90', '10', '2021-02-24 12:13:14', NULL),
(21, 20, 18, '499', '1', '489', '10', '2021-02-24 12:22:15', NULL),
(22, 21, 18, '499', '1', '489', '10', '2021-02-24 12:25:11', NULL),
(23, 22, 18, '499', '1', '489', '10', '2021-02-24 12:35:50', NULL),
(24, 23, 18, '499', '1', '489', '10', '2021-02-24 12:37:34', NULL),
(25, 24, 8, '100', '1', '90', '10', '2021-02-24 12:38:34', NULL),
(26, 25, 18, '499', '1', '489', '10', '2021-02-24 12:40:49', NULL),
(27, 26, 18, '499', '1', '489', '10', '2021-02-24 12:49:43', NULL),
(28, 27, 18, '499', '1', '489', '10', '2021-02-24 12:51:36', NULL),
(29, 28, 18, '499', '1', '489', '10', '2021-03-04 12:30:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_of` varchar(20) DEFAULT 'admin',
  `vendor_id` int(9) DEFAULT NULL,
  `franchisee_id` int(11) DEFAULT '0',
  `cat_id` int(10) DEFAULT NULL,
  `product_type` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `customer_margin_per` varchar(10) NOT NULL DEFAULT '0',
  `franchisee_margin_per` varchar(10) NOT NULL DEFAULT '0',
  `host_margin_per` varchar(10) NOT NULL DEFAULT '0',
  `box_single_qty` varchar(30) DEFAULT NULL,
  `stock` varchar(200) DEFAULT NULL,
  `desc` longtext,
  `uses` longtext,
  `featured_pro` varchar(10) DEFAULT NULL,
  `new_product` varchar(10) DEFAULT NULL,
  `deal_day` varchar(10) DEFAULT NULL,
  `related` varchar(300) DEFAULT NULL,
  `weight` varchar(100) DEFAULT NULL,
  `length` varchar(100) DEFAULT NULL,
  `breath` varchar(100) DEFAULT NULL,
  `height` varchar(100) DEFAULT NULL,
  `hazradous` varchar(20) DEFAULT NULL,
  `imported` varchar(20) DEFAULT NULL,
  `safety_warnings` text,
  `volume` varchar(100) DEFAULT NULL,
  `shelf_life` varchar(300) DEFAULT NULL,
  `hsn_code` varchar(50) DEFAULT NULL,
  `country_of_origin` varchar(50) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `brand` varchar(100) DEFAULT NULL,
  `product_id` varchar(100) DEFAULT NULL,
  `product_id_type` varchar(100) DEFAULT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `dimensions` varchar(100) DEFAULT NULL,
  `short_description` varchar(100) DEFAULT NULL,
  `net_quantity` varchar(100) DEFAULT NULL,
  `quantity` varchar(100) DEFAULT NULL,
  `mrp` varchar(100) DEFAULT NULL,
  `manufacturer` varchar(1000) DEFAULT NULL,
  `gst_type` varchar(30) DEFAULT NULL,
  `gst_per` varchar(30) DEFAULT NULL,
  `manufacturing_date` varchar(30) DEFAULT NULL,
  `is_product_expirable` varchar(30) DEFAULT NULL,
  `expiry_date` varchar(30) DEFAULT NULL,
  `age_group` varchar(30) DEFAULT NULL,
  `search_terms` varchar(500) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  `relation_type` varchar(100) DEFAULT NULL,
  `variation_theme` varchar(500) DEFAULT NULL,
  `parent_sku` varchar(500) DEFAULT NULL,
  `parentage` varchar(100) DEFAULT NULL,
  `product_json_data` longtext NOT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_of`, `vendor_id`, `franchisee_id`, `cat_id`, `product_type`, `name`, `price`, `customer_margin_per`, `franchisee_margin_per`, `host_margin_per`, `box_single_qty`, `stock`, `desc`, `uses`, `featured_pro`, `new_product`, `deal_day`, `related`, `weight`, `length`, `breath`, `height`, `hazradous`, `imported`, `safety_warnings`, `volume`, `shelf_life`, `hsn_code`, `country_of_origin`, `sku`, `brand`, `product_id`, `product_id_type`, `batch_id`, `size`, `dimensions`, `short_description`, `net_quantity`, `quantity`, `mrp`, `manufacturer`, `gst_type`, `gst_per`, `manufacturing_date`, `is_product_expirable`, `expiry_date`, `age_group`, `search_terms`, `video_url`, `relation_type`, `variation_theme`, `parent_sku`, `parentage`, `product_json_data`, `date_time`, `status`) VALUES
(1, 'admin', 0, 0, 1, NULL, 'Product 1', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(2, 'admin', 0, 0, 1, NULL, 'Product 2', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(3, 'admin', 0, 0, 1, NULL, 'Product 3', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(4, 'admin', 0, 0, 1, NULL, 'Product 4', '100', '0', '0', '0', NULL, '50', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\"></p><dd style=\"line-height: 1.42857; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\">- Lips appear full and pouty</dd><dd style=\"line-height: 1.42857; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\">- Nourishes and conditions</dd><dd style=\"line-height: 1.42857; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\">- Apply over lip liner, lipstick, or on bare lips</dd><dd style=\"line-height: 1.42857; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\">- Lips appear fuller from first application and plumper overall, over time</dd><dd style=\"line-height: 1.42857; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\">- Too Faced is a cruelty-free brand</dd><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\"></p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', '<span style=\"color: rgb(102, 102, 102); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 16px;\">Apply directly from the doe-foot applicator on clean dry lips morning and night and throughout the day as your plumping lip treatment.</span>', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(5, 'admin', 0, 0, 2, NULL, 'Product 1', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(6, 'admin', 0, 0, 2, NULL, 'Product 2', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(7, 'admin', 0, 0, 2, NULL, 'Product 3', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(8, 'admin', 0, 0, 2, NULL, 'Product 4', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(9, 'admin', 0, 0, 3, NULL, 'Product 1', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(10, 'admin', 0, 0, 3, NULL, 'Product 2', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(11, 'admin', 0, 0, 3, NULL, 'Product 3', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(12, 'admin', 0, 0, 3, NULL, 'Product 4', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(13, 'admin', 0, 0, 4, NULL, 'Product 1', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(14, 'admin', 0, 0, 4, NULL, 'Product 2', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated');
INSERT INTO `product` (`id`, `product_of`, `vendor_id`, `franchisee_id`, `cat_id`, `product_type`, `name`, `price`, `customer_margin_per`, `franchisee_margin_per`, `host_margin_per`, `box_single_qty`, `stock`, `desc`, `uses`, `featured_pro`, `new_product`, `deal_day`, `related`, `weight`, `length`, `breath`, `height`, `hazradous`, `imported`, `safety_warnings`, `volume`, `shelf_life`, `hsn_code`, `country_of_origin`, `sku`, `brand`, `product_id`, `product_id_type`, `batch_id`, `size`, `dimensions`, `short_description`, `net_quantity`, `quantity`, `mrp`, `manufacturer`, `gst_type`, `gst_per`, `manufacturing_date`, `is_product_expirable`, `expiry_date`, `age_group`, `search_terms`, `video_url`, `relation_type`, `variation_theme`, `parent_sku`, `parentage`, `product_json_data`, `date_time`, `status`) VALUES
(15, 'admin', 0, 0, 4, NULL, 'Product 3', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(16, 'admin', 0, 0, 4, NULL, 'Product 4', '100', '0', '0', '0', NULL, '50', '<p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Lip Injection Extreme is a breakthrough gloss that delivers both immediate and long-term plumping with scientifically-proven, advanced lip volumizing technologies. The glossy tinted formula naturally and dramatically hydrates, nourishes, and increases lip volume.</p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">Reasons to Love:</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear full and pouty</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Nourishes and conditions</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Apply over lip liner, lipstick, or on bare lips</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Lips appear fuller from first application and plumper overall, over time</dd><dd source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"line-height: 1.42857; color: rgb(102, 102, 102);\">- Too Faced is a cruelty-free brand</dd><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\"><b style=\"font-weight: bold;\">What else you need to know:&nbsp;</b></p><p source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(102, 102, 102);\">Too Faced Co-Founder and Chief Creative Officer Jerrod Blandino created Lip Injection Extreme so everyone could have access to plump, sexy lips without having to resort to needles. The exclusive formula launched in 2005 and became an instant icon because it\'s more than an ordinary plumper. Lip Injection Extreme is clinically proven to plump lips immediately, but what set it apart from the start was its lab-tested blend of ingredients including Atelocollagen, Marine Filling Spheres, nourishing Avocado and Jojoba oils, and antioxidant Vitamin E that give it the unique ability to offer long-term, lasting plumping effects.</p><div><br></div>', 'Uses', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Activated'),
(18, 'admin', 0, 0, 1, NULL, 'Pee Safe - Toilet Seat Sanitizer Spray', '499', '0', '0', '0', NULL, '10', '<p style=\"margin: 1em 0px; padding: 0px; border: 0px; outline-style: none; font-size: 18px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(17, 17, 17); font-family: Poppins, sans-serif; letter-spacing: 0.3px;\">Unhygienic conditions are a major cause of concern for all. These conditions are not only present in your room or kitchen, but also in your washrooms. Public restrooms of schools, offices, malls or restaurants can pose a threat of toilet borne diseases that can be harmful. With an aim to provide clean and safe urinal experience, Pee Safe’s Toilet Seat Sanitizer is here to the rescue.</p><p style=\"margin: 1em 0px; padding: 0px; border: 0px; outline-style: none; font-size: 18px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(17, 17, 17); font-family: Poppins, sans-serif; letter-spacing: 0.3px;\">Pee Safe’s Toilet Seat Sanitizier not only sanitizes the toilet seat but also fights with the foul odour present in the washroom. The toilet seat sanitizer protects you from germs and bacteria which are breeding on your toilet seat. Within a few seconds of spraying, the sanitizer works like magic by killing all the bacteria, germs and infection present on the surface, leaving behind only clean and sanitized seat. The toilet seat sanitizer comes packed with the very refreshing fragrance of mint for a odour-less experience. The product is dermatologically tested ensuring that it is safe for all skin types. For best results, spray on the surface from a distance of 25cms and wait for 10 seconds for the spray to evaporate.</p><ul style=\"margin: 1em 0px; padding: 0px 0px 0px 40px; border: 0px; outline-style: none; font-size: 18px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: rgb(17, 17, 17); font-family: Poppins, sans-serif; letter-spacing: 0.3px;\"><li style=\"margin: 0px; padding: 0px; border: 0px; outline-style: none; background: transparent; line-height: 20px;\">The Pee Safe Toilet Seat Sanitizer disinfects the toilet seat and protects you from the germs lurking around it.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline-style: none; background: transparent; line-height: 20px;\">The long lasting and uplifting fragrance of mint makes sure you don’t have to survive the torture of dirty and stinky washroom anymore. Be it home, restaurant or any public washroom, just spray Pee Safe Toilet Seat Sanitizer on your toilet seat, wait for few seconds and you are ready to use the seat that is sanitized and clean.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline-style: none; background: transparent; line-height: 20px;\">It reduces the risk of UTI and other infections. Also, can be used on other surfaces such as flush knobs, taps and toilet door handles.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline-style: none; background: transparent; line-height: 20px;\">The spray comes handy and in different sizes. It fits into even smallest of your bags. This spray takes all your washroom horrors away.</li></ul>', '', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-02-11 13:19:38', 'Activated'),
(25, 'admin', 0, 0, 7, NULL, 'Test product', '1000', '0', '0', '0', NULL, '50', 'lorem ipsum', '', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-03-10 13:07:11', 'Activated'),
(26, 'admin', 0, 0, 1, NULL, 'test product 1', '500', '0', '0', '0', NULL, '10', '', 'Product feature lorem ipsum', NULL, NULL, NULL, NULL, '10', '20', '20', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-03-10 13:15:21', 'Activated'),
(27, 'admin', 0, 0, 1, 'Single', 'test product 2', '1000', '0', '0', '0', '10', '50', '', 'Product feature lorem ipsum', NULL, NULL, NULL, NULL, '10', '20', '20', '20', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-03-10 13:15:21', 'Activated'),
(28, 'admin', 0, 0, 8, NULL, 'Clothing', '400', '0', '0', '0', NULL, '10', '', '', NULL, NULL, NULL, NULL, '0.4', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-03-12 07:35:15', 'Activated'),
(29, 'admin', 0, 0, 8, NULL, 'Cloth 2', '260', '0', '0', '0', NULL, '10', '', '', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-03-12 07:35:48', 'Activated'),
(30, 'admin', 0, 0, 8, NULL, 'cloth 3', '100', '0', '0', '0', NULL, '3', '', '', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-03-12 07:37:10', 'Activated'),
(31, 'admin', 0, 0, 1, 'Multi Pack', 'Test New Product', '100', '10', '5', '8', '11', '10', '', '', NULL, NULL, NULL, NULL, '100', '10', '10', '10', 'Yes', 'No', 'this safe warn', '100', 'before 14 days', '101035', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 11:21:59', 'Activated'),
(39, 'vendor', 3, 0, 1, 'Single', 'Lifebuoy Handwash Fights Bacteria & Viruses Refill Pouch, 5 ltr', '750', '0', '0', '0', '10', '', 'Lifebuoy, Worldâ€™s number 1 selling germ protection soap, brings to you Lifebuoy Total 10 Handwash - Activ Silver Formula,  that gives you 99.9% germ protection* with every wash. \n\nOur hands touch multiple surfaces through the day - causing hands to gather bacteria & viruses that may lead to infections. Simple hand hygiene habits for your family can be the first line of defence in protection from various infection causing germs. Washing hands with soap or handwash instead of washing hands with just plain water is recommended. This can help protect you and your family from ingesting infection causing germs that are sitting on your hands. \n\nLifebuoy Handwash is easy to use. Itâ€™s creamy liquid formula spreads easily and washes away germs from the corners of your hands. \n\nThe World Health Organisation (W.H.O.) says that frequent handwashing with soap is one of the most effective means of preventing the spread of bacteria and viruses. It\'s always good to use soap when available, but when this isn\'t possible,  use Lifebuoy Hand Sanitizer when out of home.\n*as per lab test', 'Lifebuoy Total 10 Handwash is an effective way to fight infection causing bacteria and viruses Lifebuoy Total 10 hand wash, Activ Silver Formula - the powerful germ protection formula to fight infection-causing germs This enables Lifebuoy to give 99.9% germ protection Lifebuoy Total 10 Handwash is accredited by Royal Society for Public Health (RSPH), UKLifebuoy Handwash is easy to use, spreads easily and washes away germs from the corner of your hands Wash your hands thoroughly with Lifebuoy Total 10 Handwash for protection from bacteria and viruses Store in a cool & dry placeKeep germs away from your hands with lifebuoy hand wash. Take a few drops of the liquid handwash, add water and rub the hands together to create  lather. Rub the back of your hands as well as your fingertips and nails. Rinse with water and dry with a clean cloth', NULL, NULL, NULL, NULL, '5500', NULL, NULL, NULL, 'No', 'No', '', '5000', '18 months', '3401.30.19', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:24:45', 'Activated'),
(40, 'vendor', 3, 0, 1, 'Single', 'Lifebuoy Antibacterial Germ Kill Spray (No Gas Sanitizer Spray) â€“ Safe On Skin, Safe On Surfaces |', '199', '0', '0', '0', '10', '', 'While we always remember to sanitize our hands, we often forget about germs on surfaces that are frequently touched. Mobile phones, restaurant tables, door knobs or toys that kids play with may also carry harmful bacteria & viruses.\nThe Worldâ€™s Number 1 Germ Protection Brand, Lifebuoy, brings to you its first ever Germ Kill Spray, with NO GAS formula. It instantly kills 99.9% bacteria and viruses. It can be used as often as required and works without water, a must have at home or on the go. Whatâ€™s even better â€“ it is safe on skin!\nThis antibacterial, no gas formula, comes in a spill proof pack and guarantees 1100 sprays. It is easy to carry so you can protect yourself at all times.  Best to use it on frequently touched surfaces to kill the unseen bacteria and viruses. Just hold it upright, close to the pre-cleaned surface, spray all over and wait until dry. Always test for suitability ona small surface/mobile phone prior to use. \nAlso try our range of Lifebuoy Total Hand Sanitizers, available in convenient sizes.', 'Instant Germ Kill Spray that kills 99.9% bacteria and viruses (*as per lab test on representative organism)India\'s First Lifebuoy Germ Kill spray, with no gas formula, gives 1100 sprays guaranteed Eliminates illness causing bacteria and viruses (*as per lab test on representative organism)Safe to use on skin & surfaces (^Do not use on copper, brass, painted or lacquered or varnished surfaces, acrylic plastics & electrical devices. Always test for suitability on small surface/mobile phone prior to use)Easy to carry anywhere, gives on the go protection No wiping required and comes in a refreshing fragranceKeep away from heat and flame', NULL, NULL, NULL, NULL, '208', NULL, NULL, NULL, 'No', 'No', '', '200', '24 months', '3808.94.00', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:24:45', 'Activated'),
(41, 'vendor', 3, 0, 1, 'Single', 'Lifebuoy Alcohol Based Germ Protection Hand Sanitizer 5Lt Liquid', '1250', '0', '0', '0', '10', '', 'When we step out of home, our hands touch common surfaces like elevator buttons, cab doors, train handles or grocery carts that are a breeding ground of bacteria & viruses. However we donâ€™t always have access to water & soap. Â \n\nThatâ€™s why Lifebuoy Total hand sanitizer, is a must have when you are out of home.  \n\nThe world health organisation recommends to use an alcohol based hand sanitizer when out of home to prevent the spread of bacteria and virus. \nWhen out of home, use Lifebuoy alcohol based hand sanitizer, that has more than 60% alcohol content, and gives you instant germ protection without water. \n\nLifebuoy antibacterial hand sanitizer gel has glycerine, that leaves your skin feeling fresh & supple. This quick-drying, non-sticky formula also comes in a convenient size pack. It can also be used by kids when they are out of home. \n\nMake Lifebuoy hand sanitiser your new 24 hour companion in school, at home, work place, metros, market and gyms\n\nClaim support, as per lab test. \nKeep out of reach of children below 5 years. \n', 'Lifebuoy Antibacterial Hand Sanitizer has over 60% alcohol content and gives you instant germ protectionLifebuoy Antibacterial Hand Sanitizer kills 99.9% bacteria and virus without waterLifebuoy Antibacterial Hand Sanitizer has Glycerine, it leaves your skin feeling moisturised Apply enough Hand sanitiser gel on your palm. Spread and rub over the back of your hands thoroughly and fingertips until completely dry before touching other surfacesLifebuoy Antibacterial Hand Sanitizer also comes in a convenient easy to carry pack that helps protect you when out of homeMake Lifebuoy hand sanitiser your new 24 hour companion in school, at home, work place, metros, market and gymsKeep away from heat and flameApply on your palm. Spead & rub over back of your hands and fingertips until dry.', NULL, NULL, NULL, NULL, '5600', NULL, NULL, NULL, 'No', 'No', 'Caution : For external use only. Flammable. Keep away from heat & flame. Avoid eye contact. In case of eye contact, rinse eyes throughly with water. In case of irritation, discontinue use & consult a doctor. Keep out of reach of children below 5 years. If swalowed contact a doctor. ', '5000', '24 months', '3808.94.00', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:24:45', 'Activated'),
(42, 'franchisee', 0, 1, 1, 'Single', 'asb', '10', '0', '0', '0', '10', '10', '', '', NULL, NULL, NULL, NULL, '10', '10', '10', '10', 'Yes', 'Yes', 'qnfk', '10', 'k1', '1013', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:35:11', 'Activated'),
(43, 'franchisee', 0, 1, 1, 'Single', 'Lifebuoy Handwash Fights Bacteria & Viruses Refill Pouch, 5 ltr', '750', '0', '0', '0', '10', '', 'Lifebuoy, Worldâ€™s number 1 selling germ protection soap, brings to you Lifebuoy Total 10 Handwash - Activ Silver Formula,  that gives you 99.9% germ protection* with every wash. \n\nOur hands touch multiple surfaces through the day - causing hands to gather bacteria & viruses that may lead to infections. Simple hand hygiene habits for your family can be the first line of defence in protection from various infection causing germs. Washing hands with soap or handwash instead of washing hands with just plain water is recommended. This can help protect you and your family from ingesting infection causing germs that are sitting on your hands. \n\nLifebuoy Handwash is easy to use. Itâ€™s creamy liquid formula spreads easily and washes away germs from the corners of your hands. \n\nThe World Health Organisation (W.H.O.) says that frequent handwashing with soap is one of the most effective means of preventing the spread of bacteria and viruses. It\'s always good to use soap when available, but when this isn\'t possible,  use Lifebuoy Hand Sanitizer when out of home.\n*as per lab test', 'Lifebuoy Total 10 Handwash is an effective way to fight infection causing bacteria and viruses Lifebuoy Total 10 hand wash, Activ Silver Formula - the powerful germ protection formula to fight infection-causing germs This enables Lifebuoy to give 99.9% germ protection Lifebuoy Total 10 Handwash is accredited by Royal Society for Public Health (RSPH), UKLifebuoy Handwash is easy to use, spreads easily and washes away germs from the corner of your hands Wash your hands thoroughly with Lifebuoy Total 10 Handwash for protection from bacteria and viruses Store in a cool & dry placeKeep germs away from your hands with lifebuoy hand wash. Take a few drops of the liquid handwash, add water and rub the hands together to create  lather. Rub the back of your hands as well as your fingertips and nails. Rinse with water and dry with a clean cloth', NULL, NULL, NULL, NULL, '5500', NULL, NULL, NULL, 'No', 'No', '', '5000', '18 months', '3401.30.19', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:36:38', 'Activated'),
(44, 'franchisee', 0, 1, 1, 'Single', 'Lifebuoy Antibacterial Germ Kill Spray (No Gas Sanitizer Spray) â€“ Safe On Skin, Safe On Surfaces |', '199', '0', '0', '0', '10', '', 'While we always remember to sanitize our hands, we often forget about germs on surfaces that are frequently touched. Mobile phones, restaurant tables, door knobs or toys that kids play with may also carry harmful bacteria & viruses.\nThe Worldâ€™s Number 1 Germ Protection Brand, Lifebuoy, brings to you its first ever Germ Kill Spray, with NO GAS formula. It instantly kills 99.9% bacteria and viruses. It can be used as often as required and works without water, a must have at home or on the go. Whatâ€™s even better â€“ it is safe on skin!\nThis antibacterial, no gas formula, comes in a spill proof pack and guarantees 1100 sprays. It is easy to carry so you can protect yourself at all times.  Best to use it on frequently touched surfaces to kill the unseen bacteria and viruses. Just hold it upright, close to the pre-cleaned surface, spray all over and wait until dry. Always test for suitability ona small surface/mobile phone prior to use. \nAlso try our range of Lifebuoy Total Hand Sanitizers, available in convenient sizes.', 'Instant Germ Kill Spray that kills 99.9% bacteria and viruses (*as per lab test on representative organism)India\'s First Lifebuoy Germ Kill spray, with no gas formula, gives 1100 sprays guaranteed Eliminates illness causing bacteria and viruses (*as per lab test on representative organism)Safe to use on skin & surfaces (^Do not use on copper, brass, painted or lacquered or varnished surfaces, acrylic plastics & electrical devices. Always test for suitability on small surface/mobile phone prior to use)Easy to carry anywhere, gives on the go protection No wiping required and comes in a refreshing fragranceKeep away from heat and flame', NULL, NULL, NULL, NULL, '208', NULL, NULL, NULL, 'No', 'No', '', '200', '24 months', '3808.94.00', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:36:43', 'Activated'),
(45, 'franchisee', 0, 1, 1, 'Single', 'Lifebuoy Alcohol Based Germ Protection Hand Sanitizer 5Lt Liquid', '1250', '0', '0', '0', '10', '', 'When we step out of home, our hands touch common surfaces like elevator buttons, cab doors, train handles or grocery carts that are a breeding ground of bacteria & viruses. However we donâ€™t always have access to water & soap. Â \n\nThatâ€™s why Lifebuoy Total hand sanitizer, is a must have when you are out of home.  \n\nThe world health organisation recommends to use an alcohol based hand sanitizer when out of home to prevent the spread of bacteria and virus. \nWhen out of home, use Lifebuoy alcohol based hand sanitizer, that has more than 60% alcohol content, and gives you instant germ protection without water. \n\nLifebuoy antibacterial hand sanitizer gel has glycerine, that leaves your skin feeling fresh & supple. This quick-drying, non-sticky formula also comes in a convenient size pack. It can also be used by kids when they are out of home. \n\nMake Lifebuoy hand sanitiser your new 24 hour companion in school, at home, work place, metros, market and gyms\n\nClaim support, as per lab test. \nKeep out of reach of children below 5 years. \n', 'Lifebuoy Antibacterial Hand Sanitizer has over 60% alcohol content and gives you instant germ protectionLifebuoy Antibacterial Hand Sanitizer kills 99.9% bacteria and virus without waterLifebuoy Antibacterial Hand Sanitizer has Glycerine, it leaves your skin feeling moisturised Apply enough Hand sanitiser gel on your palm. Spread and rub over the back of your hands thoroughly and fingertips until completely dry before touching other surfacesLifebuoy Antibacterial Hand Sanitizer also comes in a convenient easy to carry pack that helps protect you when out of homeMake Lifebuoy hand sanitiser your new 24 hour companion in school, at home, work place, metros, market and gymsKeep away from heat and flameApply on your palm. Spead & rub over back of your hands and fingertips until dry.', NULL, NULL, NULL, NULL, '5600', NULL, NULL, NULL, 'No', 'No', 'Caution : For external use only. Flammable. Keep away from heat & flame. Avoid eye contact. In case of eye contact, rinse eyes throughly with water. In case of irritation, discontinue use & consult a doctor. Keep out of reach of children below 5 years. If swalowed contact a doctor. ', '5000', '24 months', '3808.94.00', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:36:43', 'Activated'),
(49, 'vendor', 3, 0, 1, 'Single', 'Lifebuoy Handwash Fights Bacteria & Viruses Refill Pouch, 5 ltr', '750', '0', '0', '0', '10', '', 'Lifebuoy, Worldâ€™s number 1 selling germ protection soap, brings to you Lifebuoy Total 10 Handwash - Activ Silver Formula,  that gives you 99.9% germ protection* with every wash. \n\nOur hands touch multiple surfaces through the day - causing hands to gather bacteria & viruses that may lead to infections. Simple hand hygiene habits for your family can be the first line of defence in protection from various infection causing germs. Washing hands with soap or handwash instead of washing hands with just plain water is recommended. This can help protect you and your family from ingesting infection causing germs that are sitting on your hands. \n\nLifebuoy Handwash is easy to use. Itâ€™s creamy liquid formula spreads easily and washes away germs from the corners of your hands. \n\nThe World Health Organisation (W.H.O.) says that frequent handwashing with soap is one of the most effective means of preventing the spread of bacteria and viruses. It\'s always good to use soap when available, but when this isn\'t possible,  use Lifebuoy Hand Sanitizer when out of home.\n*as per lab test', 'Lifebuoy Total 10 Handwash is an effective way to fight infection causing bacteria and viruses Lifebuoy Total 10 hand wash, Activ Silver Formula - the powerful germ protection formula to fight infection-causing germs This enables Lifebuoy to give 99.9% germ protection Lifebuoy Total 10 Handwash is accredited by Royal Society for Public Health (RSPH), UKLifebuoy Handwash is easy to use, spreads easily and washes away germs from the corner of your hands Wash your hands thoroughly with Lifebuoy Total 10 Handwash for protection from bacteria and viruses Store in a cool & dry placeKeep germs away from your hands with lifebuoy hand wash. Take a few drops of the liquid handwash, add water and rub the hands together to create  lather. Rub the back of your hands as well as your fingertips and nails. Rinse with water and dry with a clean cloth', NULL, NULL, NULL, NULL, '5500', NULL, NULL, NULL, 'No', 'No', '', '5000', '18 months', '3401.30.19', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:42:04', 'Activated'),
(50, 'vendor', 3, 0, 1, 'Single', 'Lifebuoy Antibacterial Germ Kill Spray (No Gas Sanitizer Spray) â€“ Safe On Skin, Safe On Surfaces |', '199', '0', '0', '0', '10', '', 'While we always remember to sanitize our hands, we often forget about germs on surfaces that are frequently touched. Mobile phones, restaurant tables, door knobs or toys that kids play with may also carry harmful bacteria & viruses.\nThe Worldâ€™s Number 1 Germ Protection Brand, Lifebuoy, brings to you its first ever Germ Kill Spray, with NO GAS formula. It instantly kills 99.9% bacteria and viruses. It can be used as often as required and works without water, a must have at home or on the go. Whatâ€™s even better â€“ it is safe on skin!\nThis antibacterial, no gas formula, comes in a spill proof pack and guarantees 1100 sprays. It is easy to carry so you can protect yourself at all times.  Best to use it on frequently touched surfaces to kill the unseen bacteria and viruses. Just hold it upright, close to the pre-cleaned surface, spray all over and wait until dry. Always test for suitability ona small surface/mobile phone prior to use. \nAlso try our range of Lifebuoy Total Hand Sanitizers, available in convenient sizes.', 'Instant Germ Kill Spray that kills 99.9% bacteria and viruses (*as per lab test on representative organism)India\'s First Lifebuoy Germ Kill spray, with no gas formula, gives 1100 sprays guaranteed Eliminates illness causing bacteria and viruses (*as per lab test on representative organism)Safe to use on skin & surfaces (^Do not use on copper, brass, painted or lacquered or varnished surfaces, acrylic plastics & electrical devices. Always test for suitability on small surface/mobile phone prior to use)Easy to carry anywhere, gives on the go protection No wiping required and comes in a refreshing fragranceKeep away from heat and flame', NULL, NULL, NULL, NULL, '208', NULL, NULL, NULL, 'No', 'No', '', '200', '24 months', '3808.94.00', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:42:04', 'Activated'),
(51, 'vendor', 3, 0, 1, 'Single', 'Lifebuoy Alcohol Based Germ Protection Hand Sanitizer 5Lt Liquid', '1250', '0', '0', '0', '10', '', 'When we step out of home, our hands touch common surfaces like elevator buttons, cab doors, train handles or grocery carts that are a breeding ground of bacteria & viruses. However we donâ€™t always have access to water & soap. Â \n\nThatâ€™s why Lifebuoy Total hand sanitizer, is a must have when you are out of home.  \n\nThe world health organisation recommends to use an alcohol based hand sanitizer when out of home to prevent the spread of bacteria and virus. \nWhen out of home, use Lifebuoy alcohol based hand sanitizer, that has more than 60% alcohol content, and gives you instant germ protection without water. \n\nLifebuoy antibacterial hand sanitizer gel has glycerine, that leaves your skin feeling fresh & supple. This quick-drying, non-sticky formula also comes in a convenient size pack. It can also be used by kids when they are out of home. \n\nMake Lifebuoy hand sanitiser your new 24 hour companion in school, at home, work place, metros, market and gyms\n\nClaim support, as per lab test. \nKeep out of reach of children below 5 years. \n', 'Lifebuoy Antibacterial Hand Sanitizer has over 60% alcohol content and gives you instant germ protectionLifebuoy Antibacterial Hand Sanitizer kills 99.9% bacteria and virus without waterLifebuoy Antibacterial Hand Sanitizer has Glycerine, it leaves your skin feeling moisturised Apply enough Hand sanitiser gel on your palm. Spread and rub over the back of your hands thoroughly and fingertips until completely dry before touching other surfacesLifebuoy Antibacterial Hand Sanitizer also comes in a convenient easy to carry pack that helps protect you when out of homeMake Lifebuoy hand sanitiser your new 24 hour companion in school, at home, work place, metros, market and gymsKeep away from heat and flameApply on your palm. Spead & rub over back of your hands and fingertips until dry.', NULL, NULL, NULL, NULL, '5600', NULL, NULL, NULL, 'No', 'No', 'Caution : For external use only. Flammable. Keep away from heat & flame. Avoid eye contact. In case of eye contact, rinse eyes throughly with water. In case of irritation, discontinue use & consult a doctor. Keep out of reach of children below 5 years. If swalowed contact a doctor. ', '5000', '24 months', '3808.94.00', 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-05-02 14:42:04', 'Activated'),
(52, 'admin', 3, 0, 1, 'Single', 'Test Update Excel Upload', '100', '10', '20', '15', '1', '25', 'This is descriptionThis long desc', 'testtest2test 3test4ABCTESTTESTTEST', NULL, NULL, NULL, NULL, '10kg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10205', 'India', 'ABC1010', 'Pee Safe', '194194', 'EAN', '250250', '10', '10*20*10', NULL, '10', '25', '140', 'TEST', 'GST', '18', '10-20-19', 'YES', '01-24-22', '', 'ABC,LMN', '', 'Variation', 'Size,Color', 'ABC10404', 'Parent', '', '2021-05-11 10:58:20', 'Activated'),
(53, 'admin', 3, 0, 1, 'Single', 'Test Update Check Excel Upload', '100', '10', '20', '15', '1', '25', 'This is descriptionThis long desc', 'testtest2test 3test4ABCTESTTESTTEST', NULL, NULL, NULL, NULL, '10kg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10205', 'India', 'ABC1010259', 'Pee Safe', '194194', 'EAN', '250250', '10', '10*20*10', NULL, '10', '25', '140', 'TEST', 'GST', '18', '10-20-19', 'YES', '01-24-22', '', 'ABC,LMN', '', 'Variation', 'Size,Color', 'ABC10404', 'Parent', '', '2021-05-11 11:58:39', 'Activated');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(20) NOT NULL,
  `product_id` int(20) DEFAULT NULL,
  `img` varchar(300) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `img`, `date_time`, `status`) VALUES
(19, 1, 'Product_1_21611990064.jpg', '2021-01-30 07:01:04', NULL),
(27, 2, 'Product_2_21611990195.jpg', '2021-01-30 07:03:15', NULL),
(25, 3, 'Product_3_21611990155.jpg', '2021-01-30 07:02:35', NULL),
(17, 4, 'Product_4_11611989570.jpg', '2021-01-30 06:52:50', NULL),
(42, 12, 'Product_4_21611990613.jpg', '2021-01-30 07:10:13', NULL),
(35, 6, 'Product_2_21611990315.jpg', '2021-01-30 07:05:15', NULL),
(33, 7, 'Product_3_21611990290.jpg', '2021-01-30 07:04:50', NULL),
(29, 8, 'Product_4_21611990230.jpg', '2021-01-30 07:03:50', NULL),
(47, 9, 'Product_1_21611990713.jpg', '2021-01-30 07:11:53', NULL),
(45, 10, 'Product_2_21611990678.jpg', '2021-01-30 07:11:18', NULL),
(43, 11, 'Product_3_21611990647.jpg', '2021-01-30 07:10:47', NULL),
(41, 12, 'Product_4_11611990613.jpg', '2021-01-30 07:10:13', NULL),
(55, 13, 'Product_1_21611990850.jpg', '2021-01-30 07:14:10', NULL),
(53, 14, 'Product_2_21611990821.jpg', '2021-01-30 07:13:41', NULL),
(51, 15, 'Product_3_21611990789.jpg', '2021-01-30 07:13:09', NULL),
(49, 16, 'Product_4_21611990756.jpg', '2021-01-30 07:12:36', NULL),
(18, 4, 'Product_4_21611989570.jpg', '2021-01-30 06:52:50', NULL),
(20, 1, 'Product_1_31611990064.jpg', '2021-01-30 07:01:04', NULL),
(21, 1, 'Product_1_41611990064.jpg', '2021-01-30 07:01:04', NULL),
(22, 1, 'Product_1_21611990079.jpg', '2021-01-30 07:01:19', NULL),
(23, 1, 'Product_1_31611990079.jpg', '2021-01-30 07:01:19', NULL),
(24, 1, 'Product_1_41611990079.jpg', '2021-01-30 07:01:19', NULL),
(26, 3, 'Product_3_31611990155.jpg', '2021-01-30 07:02:35', NULL),
(28, 2, 'Product_2_31611990195.jpg', '2021-01-30 07:03:15', NULL),
(30, 8, 'Product_4_31611990230.jpg', '2021-01-30 07:03:50', NULL),
(31, 8, 'Product_4_21611990241.jpg', '2021-01-30 07:04:01', NULL),
(32, 8, 'Product_4_31611990241.jpg', '2021-01-30 07:04:01', NULL),
(34, 7, 'Product_3_31611990290.jpg', '2021-01-30 07:04:50', NULL),
(36, 6, 'Product_2_31611990315.jpg', '2021-01-30 07:05:15', NULL),
(37, 6, 'Product_2_21611990339.jpg', '2021-01-30 07:05:39', NULL),
(38, 6, 'Product_2_31611990339.jpg', '2021-01-30 07:05:39', NULL),
(39, 5, 'Product_1_11611990376.jpg', '2021-01-30 07:06:16', NULL),
(40, 5, 'Product_1_21611990376.jpg', '2021-01-30 07:06:16', NULL),
(44, 11, 'Product_3_31611990647.jpg', '2021-01-30 07:10:47', NULL),
(46, 10, 'Product_2_31611990678.jpg', '2021-01-30 07:11:18', NULL),
(48, 9, 'Product_1_31611990713.jpg', '2021-01-30 07:11:53', NULL),
(50, 16, 'Product_4_31611990756.jpg', '2021-01-30 07:12:36', NULL),
(52, 15, 'Product_3_31611990789.jpg', '2021-01-30 07:13:09', NULL),
(54, 14, 'Product_2_31611990821.jpg', '2021-01-30 07:13:41', NULL),
(56, 13, 'Product_1_31611990850.jpg', '2021-01-30 07:14:10', NULL),
(59, 0, 'TEST_11612331830.jpg', '2021-02-03 05:57:10', NULL),
(60, 17, 'Pee_Safe_Aloe_Vera_Panty_Liners_11613049425.jpg', '2021-02-11 13:17:05', NULL),
(61, 18, 'Pee_Safe_-_Toilet_Seat_Sanitizer_Spray_11613049578.jpg', '2021-02-11 13:19:38', NULL),
(62, 19, 'Bigbackpack_11613049781.jpg', '2021-02-11 13:23:01', NULL),
(63, 20, 'Classic_Shopping_Bag_11613049872.jpg', '2021-02-11 13:24:32', NULL),
(64, 23, 'p1.jpg', '2021-03-04 13:45:34', NULL),
(65, 23, 'p2.jpg', '2021-03-04 13:45:34', NULL),
(66, 23, 'p3.jpg', '2021-03-04 13:45:34', NULL),
(67, 24, 'p1.jpg', '2021-03-04 13:45:34', NULL),
(68, 24, 'p2.jpg', '2021-03-04 13:45:34', NULL),
(69, 24, 'p3.jpg', '2021-03-04 13:45:34', NULL),
(70, 25, 'Test_product_11615381631.jpg', '2021-03-10 13:07:11', NULL),
(71, 26, '', '2021-03-10 13:15:21', NULL),
(73, 27, 'test_product_2_11615382186.jpg', '2021-03-10 13:16:26', NULL),
(74, 27, 'test_product_2_21615382186.jpg', '2021-03-10 13:16:26', NULL),
(75, 28, 'Clothing_11615534515.jpg', '2021-03-12 07:35:15', NULL),
(76, 29, 'Cloth_2_11615534548.jpg', '2021-03-12 07:35:48', NULL),
(77, 30, 'cloth_3_11615534630.jpg', '2021-03-12 07:37:10', NULL),
(78, 30, 'cloth_3_21615534630.jpg', '2021-03-12 07:37:10', NULL),
(79, 0, '', '2021-05-02 12:09:06', NULL),
(80, 0, '', '2021-05-02 12:09:06', NULL),
(81, 0, 'India', '2021-05-02 12:09:06', NULL),
(82, 0, '', '2021-05-02 12:10:38', NULL),
(83, 0, '', '2021-05-02 12:10:38', NULL),
(84, 0, 'India', '2021-05-02 12:10:38', NULL),
(85, 0, '', '2021-05-02 12:11:04', NULL),
(86, 0, '', '2021-05-02 12:11:04', NULL),
(87, 32, 'India', '2021-05-02 12:11:04', NULL),
(88, 33, '', '2021-05-02 12:14:25', NULL),
(89, 34, '', '2021-05-02 12:14:25', NULL),
(90, 35, 'India', '2021-05-02 12:14:25', NULL),
(91, 36, '', '2021-05-02 14:08:36', NULL),
(92, 37, '', '2021-05-02 14:08:36', NULL),
(93, 38, 'India', '2021-05-02 14:08:36', NULL),
(94, 39, '', '2021-05-02 14:24:45', NULL),
(95, 40, '', '2021-05-02 14:24:45', NULL),
(96, 41, 'India', '2021-05-02 14:24:45', NULL),
(97, 43, '', '2021-05-02 14:36:43', NULL),
(98, 44, '', '2021-05-02 14:36:43', NULL),
(99, 45, 'India', '2021-05-02 14:36:43', NULL),
(100, 46, '', '2021-05-02 14:41:21', NULL),
(101, 47, '', '2021-05-02 14:41:21', NULL),
(102, 48, 'India', '2021-05-02 14:41:22', NULL),
(103, 49, '', '2021-05-02 14:42:04', NULL),
(104, 50, '', '2021-05-02 14:42:04', NULL),
(105, 51, 'India', '2021-05-02 14:42:04', NULL),
(116, 0, 'http://testimage.jpg', '2021-05-11 11:22:34', NULL),
(117, 0, '', '2021-05-11 11:22:34', NULL),
(111, 0, 'image.jpg', '2021-05-11 11:05:00', NULL),
(112, 0, '', '2021-05-11 11:05:00', NULL),
(113, 0, '', '2021-05-11 11:05:00', NULL),
(114, 0, '', '2021-05-11 11:05:00', NULL),
(115, 0, '', '2021-05-11 11:05:00', NULL),
(118, 0, '', '2021-05-11 11:22:34', NULL),
(119, 0, '', '2021-05-11 11:22:34', NULL),
(120, 0, '', '2021-05-11 11:22:34', NULL),
(121, 52, 'http://testimage.jpg', '2021-05-11 11:24:39', NULL),
(122, 52, '', '2021-05-11 11:24:39', NULL),
(123, 52, '', '2021-05-11 11:24:39', NULL),
(124, 52, '', '2021-05-11 11:24:39', NULL),
(125, 52, '', '2021-05-11 11:24:39', NULL),
(137, 53, 'https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043', '2021-05-13 06:33:44', NULL),
(138, 0, 'https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043', '2021-05-13 06:35:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_charges`
--

CREATE TABLE `shipping_charges` (
  `id` int(9) NOT NULL,
  `shipping_charges` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `free_shipping_above` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_charges`
--

INSERT INTO `shipping_charges` (`id`, `shipping_charges`, `free_shipping_above`, `date_time`, `status`) VALUES
(1, '100', '2000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(9) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `woloo_margin_per` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_margin_per` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `franchisee_margin_per` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `host_margin_per` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `name`, `image`, `woloo_margin_per`, `customer_margin_per`, `franchisee_margin_per`, `host_margin_per`, `username`, `password`, `date_time`, `status`) VALUES
(0, 'Woloo', '1619931022', '10', '10', '10', '10', 'woloo', '123', '2021-05-02 10:20:22', 'Activated'),
(1, 'Abc', '1612330520download.png', '30', '50', NULL, NULL, NULL, NULL, '2021-03-11 05:09:34', 'Activated'),
(2, 'Xyz', '1612330549download.png', '20', '15', NULL, NULL, NULL, NULL, '2021-02-03 05:35:49', 'Activated'),
(3, 'Pee Safe', '1613049487', '10', '10', '5', '20', 'peesafe', '123', '2021-05-02 09:58:01', 'Activated'),
(4, 'Bagforever', '1613049790', '10', '10', NULL, NULL, NULL, NULL, '2021-02-20 12:02:01', 'Activated'),
(6, 'Clothing Vendor', '1615534370', '10', '20', NULL, NULL, NULL, NULL, '2021-03-12 07:32:50', 'Activated');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(9) NOT NULL,
  `vendor_id` int(9) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `area` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `delivery_pincodes` text,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`id`, `vendor_id`, `name`, `area`, `city`, `address`, `delivery_pincodes`, `latitude`, `longitude`, `date_time`, `status`) VALUES
(4, 3, 'Peesafe', 'Mira road', 'Mumbai', 'Mira road east', '401107,401108', '', '', '2021-05-02 13:23:07', 'Activated'),
(6, 3, 'Peesafe', 'Andheri', 'mumbai', 'andheri , mumbai', '400001,400002,400003', '', '', '2021-05-02 13:23:49', 'Activated');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_address`
--
ALTER TABLE `customer_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel_import_logs`
--
ALTER TABLE `excel_import_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banner`
--
ALTER TABLE `home_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_charges`
--
ALTER TABLE `shipping_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customer_address`
--
ALTER TABLE `customer_address`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `excel_import_logs`
--
ALTER TABLE `excel_import_logs`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `franchisee`
--
ALTER TABLE `franchisee`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_banner`
--
ALTER TABLE `home_banner`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `shipping_charges`
--
ALTER TABLE `shipping_charges`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
